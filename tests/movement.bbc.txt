REM Tests, Rachel Wil Singh
REM May 2022
REM Emulator and reference:
REM https://www.bbcbasic.co.uk/bbcbasic.html

scrw=80
scrh=25

K_UP=139
K_DOWN=138
K_LEFT=136
K_RIGHT=137

x=scrw/2
y=scrh/2
player$="X"
key=0


MODE 3

REPEAT
CLS

PRINTTAB(x,y);player$

key=GET

IF key=K_UP    THEN y=y-1
IF key=K_DOWN  THEN y=y+1
IF key=K_LEFT  THEN x=x-1
IF key=K_RIGHT THEN x=x+1

UNTIL key=8

PRINT "GOODBYE"
