REM Command tests, Rachel Wil Singh
REM May 2022
REM Emulator and reference:
REM https://www.bbcbasic.co.uk/bbcbasic.html

REM 8: BACKSPACE      9: TAB              13: ENTER
REM 128: LEFT CTRL    129: RIGHT CTRL
REM 130: HOME         131: END
REM 132: PGUP         133: PGDN
REM 134: INSERT       135: DELETE

REM 136: LEFT ARROW   137: RIGHT ARROW    138: DOWN ARROW   139: UP ARROW

REM 145: F1, 154: F10

REM 65:A, 97:a

REM escape: ESC


CLS
REPEAT

KEY=GET
PRINT "YOU ENTERED ";KEY

UNTIL KEY=8

PRINT "BYE"

